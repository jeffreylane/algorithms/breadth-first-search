package bfs

type (
	Node[T any] struct {
		Value T
		Edge  []*Node[T]
	}
	Queue[T any] struct {
		first, last *item[T]
	}
	item[T any] struct {
		next  *item[T]
		value T
	}
)

// Pushes value(s) onto the queue. FIFO
func (q *Queue[T]) Push(value ...T) {
	for _, v := range value {
		ptr := &item[T]{value: v}
		if q.last == nil {
			q.first, q.last = ptr, ptr
		} else {
			q.last, q.last.next = ptr, ptr
		}
	}
}

// Pops the first T off of the queue and returns a pointer to the value
//
// Returns nil if queue is empty
func (q *Queue[T]) Pop() *T {
	if q.first != nil {
		f := q.first.value
		q.first = q.first.next
		if q.first == nil {
			q.last = nil
		}
		return &f
	}
	return nil
}

// New Node[T] Constructor
func NewNode[T any](t T) *Node[T] {
	return &Node[T]{Value: t}
}

// New Edge Constructor
func (n *Node[T]) NewEdge(node ...*Node[T]) {
	n.Edge = append(n.Edge, node...)
}

// Return Slice of Edges from n
func (n *Node[T]) Edges() []Node[T] {
	var edges []Node[T]
	for _, e := range n.Edge {
		edges = append(edges, *e)
	}
	return edges
}

// given the top level n of Node[T], search all edges downward until
// the matching node is returned, or nothing is found
//
// key extracts a key to add to the searched map. See example for usage.
func (n *Node[T]) FindNearest(key func(Node[T]) string, match func(Node[T]) bool) *Node[T] {
	// A hash table to track searches so we don't get caught in an infinite loop
	searched := map[any]bool{}
	// First, rule out the top level node (e.g., You) from being searched.
	searched[key(*n)] = true
	q := Queue[Node[T]]{}
	q.Push(n.Edges()...)
	for {
		node := q.Pop()
		if node == nil {
			break
		}
		key := key(*node)
		if _, exists := searched[key]; !exists {
			if match(*node) {
				return node
			} else {
				q.Push(node.Edges()...)
				// The value doesn't matter, just the key in this case.
				searched[key] = true
			}
		}
	}
	return nil
}

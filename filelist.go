package bfs

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	qs "gitlab.com/jeffreylane/algorithms/quick-sort"
)

type ExcludedItems []string

func PrintFileNames(rootDirectory string, excludedItems *ExcludedItems) error {
	q := Queue[Node[string]]{}
	rd := Node[string]{Value: rootDirectory}
	q.Push(rd)
	for {
		dir := q.Pop()
		if dir == nil {
			return nil
		}
		items, err := os.ReadDir(dir.Value)
		if err != nil {
			return err
		}
		if len(items) == 0 {
			return nil
		}
		lessthan := func(a, b fs.DirEntry) bool {
			return a.Name() < b.Name()
		}
		min, max := 0, len(items)-1
		sortedItems := qs.QuickSort(items, min, max, lessthan)
		for _, item := range sortedItems {
			if excludedItems.IsInList(item.Name()) {
				continue
			}
			path := filepath.Join(dir.Value, item.Name())
			if !item.IsDir() {
				fmt.Println(path)
			} else {
				subdir := Node[string]{Value: path}
				q.Push(subdir)
			}

		}
	}
}

func (e *ExcludedItems) IsInList(item string) bool {
	if e == nil {
		return false
	}
	for _, ei := range *e {
		if string(ei) == item {
			return true
		}
	}
	return false
}

package main

import (
	"flag"
	"fmt"

	fl "gitlab.com/jeffreylane/algorithms/bread-first-search"
)

var rootDir *string = flag.String("rootDir", "../..", "The directory whose files (and subdirectory files) you want to list")

func main() {
	flag.Parse()
	fmt.Println("File List Example")
	ei := &fl.ExcludedItems{".git", ".vscode", ".gitignore"}
	err := fl.PrintFileNames(*rootDir, ei)
	if err != nil {
		fmt.Println(err.Error())
	}

}

package main

import (
	"fmt"

	bfs "gitlab.com/jeffreylane/algorithms/bread-first-search"
)

func main() {
	fmt.Println("Breadth First Search Example")
	// Demo using the generic queue
	demoQueue()

	// Demo building a Graph, e.g., similar to Facebook with First degree
	// and second degree relationships, i.e., Friends and Friends of Friends...
	Jeff := PopulateFriendsForJeff()
	// Given Jeff's Friend List (Edges, Find the fastest path to Mike)
	nameToFind := "Mike"
	findPersonByName(nameToFind, Jeff)
	findPersonByOccupation(Drummer, Jeff)
	findPersonByOccupation(Guitarist, Jeff)
}

func findPersonByName(name string, personWithFriends *bfs.Node[Friend]) {
	match := func(friend bfs.Node[Friend]) bool {
		return friend.Value.Name == name
	}
	key := func(friend bfs.Node[Friend]) string {
		return friend.Value.Name
	}
	result := personWithFriends.FindNearest(key, match)
	if result != nil {
		fmt.Printf("%v has a connection with %v\n", personWithFriends.Value.Name, result.Value.Name)
	} else {
		fmt.Printf("%v doesn't have any connections leading to %v\n", personWithFriends.Value.Name, name)
	}

}
func findPersonByOccupation(occupation string, personWithFriends *bfs.Node[Friend]) {
	match := func(friend bfs.Node[Friend]) bool {
		return friend.Value.Occupation == occupation
	}
	key := func(friend bfs.Node[Friend]) string {
		return friend.Value.Name
	}
	result := personWithFriends.FindNearest(key, match)
	if result != nil {
		fmt.Printf("%v is the nearest %v connected to %v\n", result.Value.Name, result.Value.Occupation, personWithFriends.Value.Name)
	} else {
		fmt.Printf("%v doesn't have any %v connections\n", personWithFriends.Value.Name, occupation)
	}

}

func demoQueue() {
	q := bfs.Queue[string]{}
	fmt.Println("Adding Jeff, San, and Squeaky to the queue")
	q.Push("Jeff", "San", "Squeaky")
	fmt.Println("popping and printing items off of queue until it is empty")
	for {
		if v := q.Pop(); v != nil {
			fmt.Printf("%v was removed from the queue.\n", *v)
		} else {
			break
		}
	}
}

const (
	Guitarist string = "Guitarist"
	Drummer   string = "Drummer"
	Mixer     string = "Mixer"
	Singer    string = "Singer"
	Bassist   string = "Bassist"
	Realtor   string = "Realtor"
)

type Friend struct {
	Name       string
	Occupation string
}

func NewNode[T any](t T) *bfs.Node[T] {
	return bfs.NewNode(t)
}

func NewFriend(name, occupation string) Friend {
	return Friend{Name: name, Occupation: occupation}
}

func CreateProfile(name, occupation string) *bfs.Node[Friend] {
	profile := NewFriend(name, occupation)
	return NewNode(profile)
}

// Construct a graph similar to Facebook Friends
func PopulateFriendsForJeff() *bfs.Node[Friend] {
	Jeff := CreateProfile("Jeff", Guitarist)
	Rich := CreateProfile("Rich", Mixer)
	San := CreateProfile("San", Singer)
	Fredrik := CreateProfile("Fredrik", Guitarist)
	Yann := CreateProfile("Yann", Singer)
	Leo := CreateProfile("Leo", Drummer)
	Billy := CreateProfile("Billy", Bassist)
	Mike := CreateProfile("Mike", Drummer)
	Steve := CreateProfile("Steve", Guitarist)
	Madelaine := CreateProfile("Madelaine", Realtor)
	Jeff.NewEdge(San, Rich, Madelaine, Leo)
	San.NewEdge(Rich, Yann, Madelaine)
	Rich.NewEdge(Jeff, San, Fredrik)
	Madelaine.NewEdge(Steve, Jeff, San)
	Leo.NewEdge(Jeff, Yann, Fredrik)
	Fredrik.NewEdge(Leo, Mike, Rich)
	Mike.NewEdge(Billy, Fredrik)
	Billy.NewEdge(Mike)

	return Jeff

}
